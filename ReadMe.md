<h2 align="left">Software Developer<br>Systems Programming + C,Rust,Go,Typescript + Web Technologies + GNU/Linux + Neovim + FOSS</h2>


<div align="left">
  <img src="https://img.shields.io/badge/Rust-000000?logo=rust&logoColor=white&style=for-the-badge" height="30" alt="rust logo"  />
  <img width="12" />
  <img src="https://cdn.simpleicons.org/go/00ADD8" height="30" alt="go logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" height="30" alt="typescript logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg" height="30" alt="react logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original.svg" height="30" alt="postgresql logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original.svg" height="30" alt="docker logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linux/linux-original.svg" height="30" alt="linux logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg" height="30" alt="nodejs logo"  />
  <img width="12" />
  <img src="https://cdn.simpleicons.org/debian/A81D33" height="30" alt="debian logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=neovim" height="30" alt="neovim logo"  />
</div>

###

<div align="left">
  <a href="https://www.linkedin.com/in/mir-saheb-ali/" target="_blank">
    <img src="https://img.shields.io/static/v1?message=LinkedIn&logo=linkedin&label=&color=0077B5&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="linkedin logo"  />
  </a>
  <a href="mirsahebali@protonmail.com" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Protonmail&logo=tutanota&label=&color=840010&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="tutanota logo"  />
  </a>
  <a href="https://www.youtube.com/@buildln" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Youtube&logo=youtube&label=&color=FF0000&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="youtube logo"  />
  </a>
</div>

###

<br clear="both">

<img src="https://raw.githubusercontent.com/mirsahebali/mirsahebali/output/snake.svg" alt="Snake animation" />

###